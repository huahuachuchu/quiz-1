package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.awt.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Test
    void should_great_once_game() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games"))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.content().string("success"));
    }

    @Test
    void should_use_header_return_adress() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games/2"))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.content().string("This is 2 game"));
    }

    @Test
    void should_return_status_and_content() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/next/2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"id\":\"2\",\"answer\":\"1234\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_gameId_and_content() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"answer\":\"1234\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
}
