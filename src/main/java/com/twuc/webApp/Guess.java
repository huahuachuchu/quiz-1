package com.twuc.webApp;

public class Guess {
    private Hint hint;
    private boolean correct;

    public Guess(Hint hint, boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public Hint getHint() {
        return hint;
    }

    public boolean isCorrect() {
        return correct;
    }
}
