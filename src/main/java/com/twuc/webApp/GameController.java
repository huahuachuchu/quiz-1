package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GameController {
    @PostMapping("/api/games")
    @ResponseStatus(HttpStatus.CREATED)
    public String getRightAddress(){
        return "success";
    }

    @PostMapping("/api/games/{gameId}")
    @ResponseStatus(HttpStatus.CREATED)
    public String creatNewGame(@PathVariable int gameId){
        return "This is "+gameId+" game";
    }

    @GetMapping("/api/games/next/{gameId}")
    public ResponseEntity<Message> creatAnswer(@PathVariable int gameId){
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(new Message(gameId));
    }

    @PatchMapping("/api/games/{gameId}")
    public ResponseEntity<Message> guessAnswer(@PathVariable int gameId){
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(new Message(gameId));
    }
}
